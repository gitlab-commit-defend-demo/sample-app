# Demo

Following manually steps are needed.

## Install Redis

Following steps are needed to install Redis:
``` bash
helm install redis stable/redis \
  --namespace redis \
  --create-namespace \
  --set cluster.enabled=false \
  --set usePassword=false
``` 
